# ls-cmd

Basic `ls` command.

## Building

Requires a compiler that supports C++17. Tested on GCC 7.3, Clang 5/6, and VS 2017.

### Install Dependencies

- Microsoft Guidelines Support Library
- spdlog
  - fmt
- catch2

#### Linux

Most packages in package managers are out of date, run `./linux-build-dependencies.sh` to auto-download dependencies.

#### Windows

- Install [vcpkg](https://github.com/Microsoft/vcpkg)

```powershell
PS:\> vcpkg install --triplet x64-windows ms-gsl spdlog fmt catch2
```

### Generate Build Files

#### Linux

- If using manually installed packages, make sure to set `_DIR` vars to point to them (e.g. `-Dfmt_DIR=/path/to/fmtConfig.cmake`).
- Set `-Dmsgsl_INCLUDE_DIRS`

```sh
$ cd <project_root>
$ mkdir build && cd build && cmake -Dmsgsl_INCLUDE_DIRS="/path/to/msgsl/include" -DCMAKE_BUILD_TYPE=Debug ..
```

#### Windows

- Need to help CMake find `ms-gsl`

```powershell
PS:\> cd <project_root>
PS:\> mkdir build; cd build
PS:\> cmake -DCMAKE_BUILD_TYPE=Debug -Ax64 -DCMAKE_TOOLCHAIN_FILE="<vcpkg_root>\scripts\buildsystems\vcpkg.cmake" -Dmsgsl_INCLUDE_DIRS="<vcpkg_root>\installed\x64-windows\include" ..
```

### Compile

#### Linux

```sh
$ cd <project_root>/build
$ make -j$(nproc)
```

#### Windows

NOTE: Requires MSBuild to be in the path, usually accomplished by opening the `Developer Command Prompt for VS <version>`.

```powershell
PS:\> cd <project_root>\build
PS:\> msbuild ls_cmd.sln
```

## License

MIT