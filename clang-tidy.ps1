Param($FilePath)

workflow Clang-Tidy-Workflow {
    Param($FilesToCheck, $Parallelism)

    $ErrorActionPreference = "Continue"
    ForEach -Parallel -ThrottleLimit $Parallelism ($File in $FilesToCheck) {
        try {
            # TODO remove hardcoded paths
            clang-tidy -quiet $File -- -I"C:/Users/ihattend/Tools/vcpkg/installed/x64-windows/include" -std=c++17 /DFMT_SHARED /DWIN32 /D_WINDOWS 2>&1 | % { "$_" }
        } catch {
            # ignore
        }
    }
}

$Files = Get-ChildItem $FilePath -Filter *.*pp -Recurse | select -expand FullName
$LogicalProcessorCount = (Get-WmiObject Win32_ComputerSystem).NumberOfLogicalProcessors
$ParallelismLevel = $LogicalProcessorCount * 1.5

Write-Host "Running clang-tidy on $($Files.Count) file(s) with $ParallelismLevel thread(s)...`n"

Clang-Tidy-Workflow $Files $ParallelismLevel
