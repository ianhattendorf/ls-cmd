function(get_sanitizer_flags ret_var_name)
    set(ret_sanitizer_flags "")
    if(ENABLE_SANITIZER STREQUAL Undefined)
        string(APPEND ret_sanitizer_flags " -fsanitize=undefined")
    elseif(ENABLE_SANITIZER STREQUAL Address)
        string(APPEND ret_sanitizer_flags " -fsanitize=address")
    elseif(ENABLE_SANITIZER STREQUAL "Undefined;Address")
        string(APPEND ret_sanitizer_flags " -fsanitize=undefined -fsanitize=address")
    elseif(ENABLE_SANITIZER STREQUAL Memory)
        string(APPEND ret_sanitizer_flags " -fsanitize=memory")
        # MemorySanitizer requires that all libraries be compiled with instrumentation.
        # This includes libstdc++/libc++ and all other linked libraries.
        # See https://github.com/google/sanitizers/wiki/MemorySanitizerLibcxxHowTo
        # TODO.
    elseif(ENABLE_SANITIZER STREQUAL Thread)
        # ThreadSanitizer is similar to MemorySanitizer, probably requires library recompilation.
        string(APPEND ret_sanitizer_flags " -fsanitize=thread")
    elseif(NOT ENABLE_SANITIZER STREQUAL OFF)
        message(FATAL_ERROR "Unknown sanitizer: " ${ENABLE_SANITIZER})
    endif()

    if(NOT ENABLE_SANITIZER STREQUAL OFF)
        string(APPEND ret_sanitizer_flags " -fno-sanitize-recover=all")
    endif()

    set(${ret_var_name} ${ret_sanitizer_flags} PARENT_SCOPE)
endfunction()

function(set_global_compiler_flags)
    get_sanitizer_flags(sanitizer_flags)

    if(MSVC) # using either Visual Studio C++ or clang-cl.exe (or possibly others, unsupported)
        if(CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
            # Remove default warning level
            string(REGEX REPLACE "/W[0-4]" "" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
        endif()
        # /wd4714: System headers warn when including C4365
        # pragma warning(push, 0) and pop around headers don't mask all warnings
        # NOTE: Can't compile with /Za, as windows headers won't correctly compile
        string(APPEND CMAKE_CXX_FLAGS " /MP /WX /W4 /wd4714")

        if("${CMAKE_CXX_COMPILER_ID}" MATCHES "(Apple)?[Cc]lang") # clang-cl.exe
            string(APPEND CMAKE_CXX_FLAGS " -Wno-unused-command-line-argument")
        else() # MSVC
            string(APPEND CMAKE_CXX_FLAGS " /permissive-")
            if(MSVC_ANALYZE)
                string(APPEND CMAKE_CXX_FLAGS  " /analyze")
            endif()
        endif()
        if(MSVC_PROFILE_BUILD)
            string(APPEND CMAKE_EXE_LINKER_FLAGS  " /Profile")
        endif()
    elseif("${CMAKE_CXX_COMPILER_ID}" MATCHES "(Apple)?[Cc]lang") # using Clang/AppleClang
        # Hide exit time destructor warnings for now (logger is static per class)
        string(APPEND CMAKE_CXX_FLAGS " -Werror -Weverything -pedantic -Wno-c++98-compat -Wno-c++98-compat-pedantic -Wno-padded -Wno-exit-time-destructors -Wno-switch-enum -Wno-covered-switch-default")
        if(CMAKE_BUILD_TYPE STREQUAL "Debug")
            string(APPEND CMAKE_CXX_FLAGS " ${sanitizer_flags}")
        endif()
    elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU") # using GCC
        # Can't use -Wduplicated-branches with fmt
        # -fsanitize requires libubsan for GCC
        string(APPEND CMAKE_CXX_FLAGS " -Werror -Wall -pedantic -Wextra -Wduplicated-cond -Wno-missing-braces -Wlogical-op -Wrestrict -Wnull-dereference -Wpointer-arith -Wold-style-cast -Wuseless-cast -Wcast-qual -Wdouble-promotion -Wshadow -Wformat=2 -Wsign-conversion -Wno-format-nonliteral")
        if(CMAKE_BUILD_TYPE STREQUAL "Debug")
            string(APPEND CMAKE_CXX_FLAGS " ${sanitizer_flags}")
        endif()
        # Avoid errors with clang-tidy receiving gcc warning flags
        if(BUILD_WITH_CLANG_TIDY)
            string(APPEND CMAKE_CXX_FLAGS " -Wno-unknown-warning-option")
        endif()
        #elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel") # using Intel C++
    else()
        message(WARNING "UNKNOWN COMPILER: CXX flags not set.")
    endif()

    set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} PARENT_SCOPE)
    set(CMAKE_EXE_LINKER_FLAGS ${CMAKE_EXE_LINKER_FLAGS} PARENT_SCOPE)
endfunction()
