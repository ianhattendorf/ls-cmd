#!/bin/sh

set -eu

fmt_version='4.1.0'
spdlog_version='0.16.3'
catch_version='2.2.2'

# Final directory for dependencies
gsl_dir='vendor/GSL-master'
fmt_dir="vendor/fmt-$fmt_version"
spdlog_dir="vendor/spdlog-$spdlog_version"
catch_dir="vendor/Catch2-$catch_version"

gsl_url='https://github.com/Microsoft/GSL/archive/master.tar.gz'
fmt_url="https://github.com/fmtlib/fmt/archive/$fmt_version.tar.gz"
spdlog_url="https://github.com/gabime/spdlog/archive/v$spdlog_version.tar.gz"
catch_url="https://github.com/catchorg/Catch2/archive/v$catch_version.tar.gz"

mkdir -p vendor

if [ ! -d "$gsl_dir" ]; then
    curl -L $gsl_url | tar -xz -C vendor
fi
if [ ! -d "$fmt_dir" ]; then
    curl -L $fmt_url | tar -xz -C vendor
fi
if [ ! -d "$spdlog_dir" ]; then
    curl -L $spdlog_url | tar -xz -C vendor
fi
if [ ! -d "$catch_dir" ]; then
    curl -L $catch_url | tar -xz -C vendor
fi

cd "$fmt_dir" && mkdir -p build && cd build && cmake -DFMT_TEST=OFF .. && make install -j$(nproc) DESTDIR=../install && cd ../../..
cd "$spdlog_dir" && mkdir -p build && cd build && cmake -DSPDLOG_BUILD_TESTING=OFF .. && make install -j$(nproc) DESTDIR=../install && cd ../../..
cd "$catch_dir" && mkdir -p build && cd build && cmake -DBUILD_TESTING=OFF .. && make install -j$(nproc) DESTDIR=../install && cd ../../..

echo -n '-Dmsgsl_INCLUDE_DIRS='
find "$gsl_dir" -type d -name 'include' -exec readlink -f '{}' \;
echo -n '-Dfmt_DIR='
find "$fmt_dir/install" -type d -path '*/cmake/fmt' -exec readlink -f '{}' \;
echo -n '-Dspdlog_DIR='
find "$spdlog_dir/install" -type d -path '*/cmake/spdlog' -exec readlink -f '{}' \;
echo -n '-DCatch2_DIR='
find "$catch_dir/install" -type d -path '*/cmake/Catch2' -exec readlink -f '{}' \;
