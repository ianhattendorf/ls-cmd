#!/bin/sh

set -eu
echo "nproc: $(nproc)"

usage() {
    echo "Usage: $0 [-a <string>] [-s <string>] [-b <string>] [-t] [-r] [-n] [-h]" 1>&2
    echo '-a: Additional CMake args []'
    echo '-s: sanitizer [OFF]'
    echo '-b: build type [Debug]'
    echo '-t: build tests [false]'
    echo '-r: rm build dir first [false]'
    echo '-n: skip make [false]'
    echo '-h: help'
    exit 1
}

additional_args=''
sanitizer='OFF'
build_type='Debug'
test_mode=false
rm_build=false
skip_make=false
vendor_dir='vendor'

while getopts ":a:s:b:trnh" o; do
    case "${o}" in
        a)
            additional_args=${OPTARG}
            ;;
        s)
            sanitizer=${OPTARG}
            ;;
        b)
            build_type=${OPTARG}
            ;;
        t)
            test_mode=true
            ;;
        r)
            rm_build=true
            ;;
        n)
            skip_make=true
            ;;
        h)
            usage
            ;;
        \?)
            usage
            ;;
    esac
done

echo "Using additional args: $additional_args"
echo "Using sanitizer=$sanitizer"
echo "Build type=$build_type"
echo "Test mode: $test_mode"
echo "Removing old build dir: $rm_build"
echo ''

cmake_cmd="cmake -DCMAKE_BUILD_TYPE='$build_type' -DENABLE_SANITIZER='$sanitizer' -Dmsgsl_INCLUDE_DIRS=$(readlink -f $vendor_dir/GSL-master/include) -Dfmt_DIR=$(readlink -f $vendor_dir/fmt-4.1.0/install/usr/local/lib64/cmake/fmt) -Dspdlog_DIR=$(readlink -f $vendor_dir/spdlog-0.16.3/install/usr/local/lib64/cmake/spdlog) $additional_args .."
if [ "$test_mode" = true ]; then
    cmake_cmd="$cmake_cmd -DBUILD_TESTING=ON -DCatch2_DIR=$(readlink -f $vendor_dir/Catch2-2.2.2/install/usr/local/lib64/cmake/Catch2)"
fi

./linux-build-dependencies.sh

if [ "$rm_build" = true ] && [ -d build ]; then
    rm -r build
fi
mkdir -p build && cd build
eval $cmake_cmd
if [ "$skip_make" = false ]; then
    make -j$(nproc)
fi
