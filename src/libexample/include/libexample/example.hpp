/*!
 * \file example.hpp
 * \brief Example library to test different testing/static analysis/documentation tools
 */

#pragma once

/*!
 * \brief Calculate the factorial of an integer
 *
 * \param number Value to calculate the factorial of
 * \return Factorial of \p number
 */
int factorial(int number);
