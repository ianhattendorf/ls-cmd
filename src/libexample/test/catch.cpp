/*!
 * \file catch.cpp
 * \brief Catch main definition file
 */

#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
#include <catch.hpp>
