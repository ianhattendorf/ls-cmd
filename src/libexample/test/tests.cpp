/*!
 * \file tests.cpp
 * \brief Tests for example.hpp
 */

#include "libexample/example.hpp"
#include <catch.hpp>

TEST_CASE("Factorials are computed", "[factorial]") {
  REQUIRE(factorial(1) == 1);
  REQUIRE(factorial(2) == 2);
  REQUIRE(factorial(3) == 6);
  REQUIRE(factorial(10) == 3628800);
}
