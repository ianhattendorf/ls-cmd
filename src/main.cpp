/*!
 * \file main.cpp
 * \brief Main function definition
 */

#include "libexample/example.hpp"
#include <gsl/span>
#include <spdlog/spdlog.h>
#include <sstream>

int main(int argc, char *argv[]) {
  const auto args = gsl::make_span(argv, argc);

#ifdef NDEBUG
  spdlog::set_level(spdlog::level::info);
#else
  spdlog::set_level(spdlog::level::debug);
#endif
  const auto logger = spdlog::stdout_color_mt("main");
  logger->info("Executable location: {}", args[0]);
  logger->info("factorial(5): {}", factorial(5));

  try {
    logger->info("Success.");
  } catch (const std::exception &e) {
    logger->critical(e.what());
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
