Param(
    [Parameter(Position=0)]
    [ValidateSet('Win32','x64')]
    [System.String]$Platform
)

$ErrorActionPreference = "Stop"

switch ($Platform) {
    'Win32' {
        $triplet = 'x86-windows'
        $vcpkg_include_dir = 'c:/tools/vcpkg/installed/x86-windows/include'
    }
    'x64' {
        $triplet = 'x64-windows'
        $vcpkg_include_dir = 'c:/tools/vcpkg/installed/x64-windows/include'
    }
    default { Throw "Unknown platform: $Platform" }
}

vcpkg install --triplet $triplet ms-gsl spdlog fmt catch2
if ($LASTEXITCODE -ne 0) {
    throw "last exe exited with exit code $LASTEXITCODE"
}
mkdir build
pushd build
# Wait to use MSVC analysis until a method to exclude external libraries exists
# https://blogs.msdn.microsoft.com/vcblog/2017/12/13/broken-warnings-theory/
cmake -A $Platform -DCMAKE_BUILD_TYPE=Debug -DBUILD_TESTING=ON -DMSVC_ANALYZE=OFF -DCMAKE_TOOLCHAIN_FILE=c:/tools/vcpkg/scripts/buildsystems/vcpkg.cmake -Dmsgsl_INCLUDE_DIRS="$vcpkg_include_dir" ..
if ($LASTEXITCODE -ne 0) {
    throw "last exe exited with exit code $LASTEXITCODE"
}
popd
